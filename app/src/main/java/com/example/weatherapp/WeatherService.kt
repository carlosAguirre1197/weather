package com.example.weatherapp

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
//POST 0 crear, PATH=actualizar (varios elementos), PUT=actualizar (1 elemento)

interface WeatherService {

    @GET("data/{api_version}/weather")
    fun getWeather(
        @Path("api_version")version:String = "2.5",
        @Query("q") city:String,
        @Query("appid") appId:String = "69e1a24dd318fe83821685dc37a3d4b7"
    ):Call<WeatherResponse>

    companion object {
        val instance:WeatherService by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}